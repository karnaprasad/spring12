<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Form</title>
</head>
<body>

<h2> Customer Entry Form</h2>
<form action="addCustomer" method="post">
	<table>
		<tr><td>Customer Code</td> <td><input type="text" name="customerCode"/></td></tr>
	<tr><td>Customer Name</td> <td><input type="text" name="customerName"/></td></tr>
	<tr><td>Address</td> <td><input type="text" name="address"/></td></tr>
	<tr><td>Contact No</td> <td><input type="text" name="contactNo"/></td></tr>
	<tr><td><input type="submit" value="Save Customer"/></td></tr>
	</table>
</form>
</body>
</html>