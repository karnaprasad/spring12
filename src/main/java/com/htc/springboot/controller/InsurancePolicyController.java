package com.htc.springboot.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.htc.springboot.model.Policy;
import com.htc.springboot.service.InsuranceService;
import com.htc.springboot.to.CustomerPolicyTO;
import com.htc.springboot.to.CustomerTO;
import com.htc.springboot.to.PolicyTO;

@Controller
public class InsurancePolicyController {

	@Autowired
	InsuranceService insuranceService;
	
	//@GetMapping is specialized version of @RequestMapping annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.GET).
	//@GetMapping annotated methods handle the HTTP GET requests matched with given URI expression
	
	@RequestMapping(value="/customerForm", method=RequestMethod.GET)	
	public String showCustomerForm() {
		return "customer-form";
	}
	
	@PostMapping("/addCustomer")    //spring 4.x onwards
	public String addCustomer(@ModelAttribute(name="customer") @Valid CustomerTO customerTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if(bindingResult.hasErrors()) {
			return "customer-form";
		}
		System.out.println(customerTO);
		boolean result = insuranceService.addCustomer(customerTO);
		if(result) {
			redirectAttributes.addFlashAttribute("customerName", customerTO.getCustomerName());
			redirectAttributes.addFlashAttribute("msg", "Added successfully");
			return "redirect:/addCustomerSuccess";
		}
		else
			return "customer-form";
	}
	
	@GetMapping("/addCustomerSuccess")
	public String addCustomerSuccess() {
		return "customer-addsuccess";
	}
	
	@GetMapping("/issuePolicyForm")
	public String showIssuePolicyForm() {
		return "policy-form";
	}
	
	@PostMapping(value="/issuePolicy")
	public String issuePolicyToCustomer(@ModelAttribute CustomerPolicyTO customerPolicyTO,  RedirectAttributes attributes) {
		
		System.out.println(customerPolicyTO);
		CustomerTO customerTO = new CustomerTO(customerPolicyTO.getCustomerCode(), customerPolicyTO.getCustomerName(), customerPolicyTO.getAddress(), customerPolicyTO.getContactNo());;
		PolicyTO policyTO = new PolicyTO(customerPolicyTO.getPolicyNo(), customerPolicyTO.getPremium(), customerPolicyTO.getPolicyMode(), customerPolicyTO.getStartDate());;
		
		System.out.println(customerTO);
		System.out.println(policyTO);
		
		boolean result = insuranceService.addCustomerWithPolicy(customerTO, policyTO);
		
		if(result) {
			attributes.addFlashAttribute("policyNo", customerPolicyTO.getPolicyNo());
			attributes.addFlashAttribute("customerName", customerPolicyTO.getCustomerName());
			return "redirect:/issuePolicySuccess";
		}
		else 
			return "policy-form";
	}
	
	@GetMapping("/issuePolicySuccess")
	public String issuePolicySuccess() {
		return "issue-policysuccess";
	}
	
	@GetMapping("/searchPolicyForm")
	public String searchPolicyForm() {
		return "search-policy";
	}
	
	@PostMapping("/getPolicyDetail")
	public ModelAndView getPolicyDetails(@RequestParam(name="policyNo") long policyNo) {
		
		ModelAndView mv = new ModelAndView();
		PolicyTO policyTO = insuranceService.getInsurancePolicy(policyNo);
		if(policyTO == null) {
			mv.setViewName("no-policy");		
			mv.addObject("policyNo", policyNo);
		}
		else {
			mv.setViewName("policy-details");
			mv.addObject("policyTO", policyTO);
		}
		return mv;
	}
	@GetMapping("/searchCustomerForm")
	public String takeNewPolicyForm() {
		return "search-customer";
	}
	@PostMapping("/searchCustomer")
	public ModelAndView takeNewPolicy(@RequestParam(name="customerCode") String customerCode) {
		ModelAndView mv = new ModelAndView();
		CustomerTO customerTO=insuranceService.getCustomerDetails(customerCode);
		if(customerTO==null) {
			mv.setViewName("no-customer");
			mv.addObject("customerCode",customerCode);
		}
		else {
			mv.setViewName("take-newpolicy");
			mv.addObject("customerTO",customerTO);
		}
		return mv;
	}
	@PostMapping("/takeNewPolicy")
	public String takeNewPolicy(@ModelAttribute CustomerPolicyTO customerPolicyTO,  RedirectAttributes attributes) {
		//String customerCode=customerPolicyTO.getCustomerCode();
		CustomerTO customerTO = new CustomerTO(customerPolicyTO.getCustomerCode(), customerPolicyTO.getCustomerName(), customerPolicyTO.getAddress(), customerPolicyTO.getContactNo());
		PolicyTO policyTO = new PolicyTO(customerPolicyTO.getPolicyNo(), customerPolicyTO.getPremium(), customerPolicyTO.getPolicyMode(), customerPolicyTO.getStartDate());
		if(insuranceService.takeNewPolicy(customerTO, policyTO)) {
			System.out.println("success");
			attributes.addFlashAttribute("policyNo", customerPolicyTO.getPolicyNo());
			attributes.addFlashAttribute("customerName", customerPolicyTO.getCustomerName());
			return "redirect:/issuePolicySuccess";
		}
		else {
			System.out.println("take-newpolicy");			
		}
		return null;		
	}
	
	@GetMapping("/searchCustomerPolicy")
	public String searchCustomerPolicy() {
		return "search-customerpolicy";
	}
	
	@PostMapping("/searchCustomerPolicies")
	public ModelAndView searchCustomerPolicies(@RequestParam(name="customerCode") String customerCode) {
		ModelAndView mv = new ModelAndView();
		Set<Policy> policies = insuranceService.getInsurancePolicies(customerCode);
		if(policies==null) {
			mv.setViewName("no-policies");
			mv.addObject("customerCode",customerCode);
		}
		else {
			System.out.println(policies);
			mv.setViewName("customer-policies");
			mv.addObject("policies", policies);
		}
		return mv;
	}
}
