package com.htc.springboot.to;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class CustomerPolicyTO {
	@NotEmpty(message = "Customer code cannot be empty")
	private String customerCode;
	@NotEmpty(message = "Customer name cannot be empty")
	private String customerName;
	@NotEmpty(message = "Address is mandatory")
	@Size(min = 10, max = 50, message = "Address must be between 10 to 50 characters")
	private String address;
	@NotEmpty(message = "Phone no cannot be empty")
	@Size(min = 10, max = 10, message = "Invalid Phoneno")
	private String contactNo;

	private long policyNo;
	private double premium;
	private String policyMode;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date startDate;

	public CustomerPolicyTO() {
	}

	public CustomerPolicyTO(@NotEmpty(message = "Customer code cannot be empty") String customerCode,
			@NotEmpty(message = "Customer name cannot be empty") String customerName,
			@NotEmpty(message = "Address is mandatory") @Size(min = 10, max = 50, message = "Address must be between 10 to 50 characters") String address,
			@NotEmpty(message = "Phone no cannot be empty") @Size(min = 10, max = 10, message = "Invalid Phoneno") @NotEmpty(message = "Phone no cannot be empty") @Size(min = 10, max = 10, message = "Invalid Phoneno") @NotEmpty(message = "Phone no cannot be empty") @Size(min = 10, max = 10, message = "Invalid Phoneno") String contactNo,
			long policyNo, double premium, String policyMode, Date startDate) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.address = address;
		this.contactNo = contactNo;
		this.policyNo = policyNo;
		this.premium = premium;
		this.policyMode = policyMode;
		this.startDate = startDate;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public Long getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(Long policyNo) {
		this.policyNo = policyNo;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public String getPolicyMode() {
		return policyMode;
	}

	public void setPolicyMode(String policyMode) {
		this.policyMode = policyMode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Override
	public String toString() {
		return "CustomerPolicyTO [customerCode=" + customerCode + ", customerName=" + customerName + ", address="
				+ address + ", contactNo=" + contactNo + ", policyNo=" + policyNo + ", premium=" + premium
				+ ", policyMode=" + policyMode + ", startDate=" + startDate + "]";
	}
}
