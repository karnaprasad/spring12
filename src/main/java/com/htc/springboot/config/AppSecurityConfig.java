package com.htc.springboot.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity   
public class AppSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	PasswordEncoder passwordEncoder;

/*	@Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;  */

	@Bean
	public PasswordEncoder getPasswordEncoder() {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder;
	}


	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	auth.inMemoryAuthentication()
    	.passwordEncoder(passwordEncoder)
    	.withUser("htcuser").password(passwordEncoder.encode("123Welcome")).roles("USER")
    	.and()
    	.withUser("admin").password(passwordEncoder.encode("admin")).roles("USER", "ADMIN","EDITOR")
    	.and()
    	.withUser("newuser").password(passwordEncoder.encode("welcome")).roles("USER","EDITOR")
    	.and()
    	.withUser("user1").password(passwordEncoder.encode("12345")).roles("DEVELOPER", "MANAGER");
	}

/*	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.jdbcAuthentication()
					.passwordEncoder(passwordEncoder)
					.usersByUsernameQuery(usersQuery)
					.authoritiesByUsernameQuery(rolesQuery);
	}  */

	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
	}

	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		  http.authorizeRequests()
	  		.antMatchers("/login").permitAll()
	  		.antMatchers("/").hasRole("USER")
	  		.antMatchers("/customerForm").hasAnyRole("USER")
	  		.antMatchers("/customerFormDAO").hasAnyRole("USER")
	  		.antMatchers("/addCustomer").hasAnyRole("USER")
	  		.antMatchers("/addCustomerDAO").hasAnyRole("USER")
	  		.antMatchers("/addCustomerSucess").hasAnyRole("USER")
	  		.antMatchers("/addCustomerSucessDAO").hasAnyRole("USER")
	  		.antMatchers("/issuePolicyForm").hasAnyRole("EDITOR", "MANAGER")
	  		.antMatchers("/issuePolicyFormDAO").hasAnyRole("EDITOR", "MANAGER")
	  		.antMatchers("/issuePolicySuccess").hasAnyRole("EDITOR", "MANAGER")
	  		.antMatchers("/issuePolicySuccessDAO").hasAnyRole("EDITOR", "MANAGER")
	  	
	  		.antMatchers("/issuePolicy").hasRole("EDITOR")
	  		.antMatchers("/issuePolicyDAO").hasRole("EDITOR")
	  		.antMatchers("/getPolicyDetail").hasRole("EDITOR")
	  		.antMatchers("/getPolicyDetailDAO").hasRole("EDITOR")
	  		.antMatchers("/searchPolicyForm").hasRole("ADMIN")
	  		.antMatchers("/searchPolicyFormDAO").hasRole("ADMIN")
	  		.antMatchers("/searchCustomerForm").hasRole("ADMIN")
	  		.antMatchers("/searchCustomerFormDAO").hasRole("ADMIN")
	  		.antMatchers("/searchCustomer").hasRole("ADMIN")
	  		.antMatchers("/searchCustomerDAO").hasRole("ADMIN")
	  		.antMatchers("/takeNewPolicy").hasRole("ADMIN")
	  		.antMatchers("/takeNewPolicyDAO").hasRole("ADMIN")
	  		.antMatchers("/searchCustomerPolicy").hasRole("ADMIN")
	  		.antMatchers("/searchCustomerPolicyDAO").hasRole("ADMIN")
	  		.antMatchers("/searchCustomerPolicies").hasRole("ADMIN")
	  		.antMatchers("/searchCustomerPoliciesDAO").hasRole("ADMIN")
	  		.anyRequest()
	  		.authenticated()
		  	
		  		.and().formLogin() 
		  			.loginPage("/login")
		  			.failureUrl("/login?error=true") 
		  			.usernameParameter("username")
		  			.passwordParameter("password")
		  			.defaultSuccessUrl("/")
		  		.and()
		  		.logout()
		  			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		  			.logoutSuccessUrl("/login?logout=true")
		  		.and()
		  		.exceptionHandling()
		  			.accessDeniedPage("/Access_Denied");
	}
}
